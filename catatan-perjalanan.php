<?php  
include 'header.php';

$arr = file_get_contents($_SESSION['nik'].".txt");
$catatan = json_decode($arr, true);
?>
<!DOCTYPE html>
<html>
<head>
	<title>catatan</title>
	<style type="text/css">
		form{
			max-width: 300px;
			padding: 80px;
			font-size: 14px;
			margin: auto; 
		}

		table, th, td {
  			border: 1px solid black;
		}
		
	</style>
</head>
<body>
	<div class="card" style="margin-top: 10px; margin-left: 250px;">
		<label>Urutkan Berdasrakan</label>
		<select>
			<option value="tanggal">Tanggal</option>
			<option value="waktu">Waktu</option>
			<option value="lokasi">Lokasi</option>
			<option value="suhu">Suhu</option>
		</select>
		<button>Urutkan</button>
	</div>
	<div class="content" style="margin-top: 5px; margin-left: 250px;">
		<table width="100%">
			<tr>
				<td>Tanggal</td>
				<td>Waktu</td>
				<td>Lokasi</td>
				<td>Suhu Tubuh</td>
			</tr>
			<?php 
				foreach ($catatan as $key => $value) {
					echo "<tr>";
					echo "<td>".$value['tanggal']."</td>";
					echo "<td>".$value['waktu']."</td>";
					echo "<td>".$value['lokasi']."</td>";
					echo "<td>".$value['suhu']."</td>";
					echo "</tr>";
				}
			 ?>
		</table>
		<br>
		<a style="text-decoration: none;" href="isi-data.php">
			<button>Isi Catatan Perjalanan</button>
		</a>
	</div>
</body>
</html>