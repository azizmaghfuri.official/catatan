<?php
include 'env.php';
// MENYIMPAN DATA POST KE ARRAL
$data = [
	'nama' => $_POST['nama'],
	'nik' => $_POST['nik'],
];
// MEMBACA DATA DARI FILE siswa.txt dan disimpan di var $arr
$arr = file_get_contents("user.txt"); 
// MERUBAH DATA JSON MENJADI ARRAY $siswa
$user = json_decode($arr,true);
// MENAMBAHKAN DATA KE ARRAY $siswa
array_push($user,$data);
// MERUBAH DATA ARRAY MENJADI JSON
$json_data = json_encode($user);
// MENULIS DATA KE FILE siswa.txt
file_put_contents("user.txt", $json_data);
file_put_contents($_POST['nik'].".txt", "[]");
// REDIRECT KE HALAMAN input.php
ke("login.php");
?>