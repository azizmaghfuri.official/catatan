<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Isi Data</title>
	<link rel="stylesheet" type="text/css" href="asetku/css/style.css">
	<meta name="viewport" content="width=content-width,initial-scale=1">
</head>
<body>
			<?php include 'header.php';?>
			<div class="content">
				<form method="post" action="simpancatatan.php">
					<table class="table-input">
						<tr>
							<td>Tanggal</td>
							<td>: <input type="date" name="tanggal"></td>
						</tr>
						<tr>
							<td>Jam</td>
							<td>: <input type="time" name="waktu"></td>
						</tr>
						<tr>
							<td>Lokasi yang dikunjungi</td>
							<td>: <input type="text" name="lokasi"></td>
						</tr>
						<tr>
							<td>Suhu Tubuh</td>
							<td>: <input type="number" name="suhu"></td>
						</tr>
					</table>
					<div style="display: flex; justify-content: space-between; margin-top: 25px;">
						<button>Simpan</button>
					</div>
				</form>
			</div>
</body>
</html>