<?=
	session_start();
	date_default_timezone_set('Asia/Jakarta');

	function ke($tujuan){
		header('Location: '.$tujuan);
	}

	function akses(){
		if (!$_SESSION['login']) {
			ke('login.php');
		}
	}
?>