<?php
// MENYIMPAN DATA POST KE ARRAL
$data = [
	'nama' => $_POST['nama'],
	'NIK' => $_POST['NIK'],
];
// MEMBACA DATA DARI FILE siswa.txt dan disimpan di var $arr
$arr = file_get_contents("config.txt"); 
// MERUBAH DATA JSON MENJADI ARRAY $siswa
$user = json_decode($arr,true);

$login = false;
foreach ($user as $key => $value) {
	if ($data['nama'] == $value['nama'] && $data['NIK'] == $value['NIK']) {
		$login = true;
		break;
	}
}
if ($login) {
	session_start();
	$_SESSION['nama'] = $data['nama'];
	$_SESSION['NIK'] = $data['NIK'];
	header("Location: home.php");
} else {
	header("Location: login.php");
}

// header("Location: login.php");
?>