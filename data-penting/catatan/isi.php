<?php
session_start();
if (!isset($_SESSION['nama'])) {
	header("Location: login.php");
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>HOME</title>
</head>
<body>
	<div class="container">
		<div class="header">
			<h1>PEDULI DIRI</h1>
			<p>Catatan Perjalanan</p>
			<p>
				<a href="home.php">HOME</a> |
				<a href="catatan.php">Catatan Perjalanan</a> |
				<a href="isi.php">Isi Data |</a>
				<a href="logout.php">Logout</a>
			</p>
		</div>
		<div class="body">
			<p>
				<form method="POST" action="simpancatatan.php">
				<input type="date" name="tanggal" placeholder="TANGGAL">
				<input type="time" name="jam" placeholder="JAM">
				<input type="text" name="lokasi" placeholder="LOKASI">
				<input type="number" name="suhu" placeholder="SUHU">
				<input type="submit" name="submit" value="SIMPAN">
			</form>
			</p>
		</div>
		<div class="footer">
			
		</div>
	</div>
</body>
</html>