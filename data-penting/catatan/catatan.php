<?php
session_start();
if (!isset($_SESSION['nama'])) {
	header("Location: login.php");
}
$arr = file_get_contents($_SESSION['NIK'].".txt"); 
$catatan = json_decode($arr,true);
?>

<!DOCTYPE html>
<html>
<head>
	<title>HOME</title>
</head>
<body>
	<div class="container">
		<div class="header">
			<h1>PEDULI DIRI</h1>
			<p>Catatan Perjalanan</p>
			<p>
				<a href="home.php">HOME</a> |
				<a href="catatan.php">Catatan Perjalanan</a> |
				<a href="isi.php">Isi Data |</a>
				<a href="logout.php">Logout</a>
			</p>
		</div>
		<div class="body">
			<p>
				<table>
					<tr>
						<td>Tanggal</td>
						<td>Jam</td>
						<td>Lokasi</td>
						<td>Suhu Tubuh</td>
					</tr>
					<?php
						foreach ($catatan as $key => $value) {
							echo "<tr>";
							echo "<td>".$value['tanggal']."</td>";
							echo "<td>".$value['jam']."</td>";
							echo "<td>".$value['lokasi']."</td>";
							echo "<td>".$value['suhu']."</td>";
							echo "</tr>";
						}
					?>
				</table>
			</p>
			<a href="isi.php">Isi Catatan Perjalanan</a>
		</div>
		<div class="footer">
			
		</div>
	</div>
</body>
</html>