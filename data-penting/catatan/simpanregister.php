<?php
// MENYIMPAN DATA POST KE ARRAL
$data = [
	'nama' => $_POST['nama'],
	'NIK' => $_POST['NIK'],
];
// MEMBACA DATA DARI FILE siswa.txt dan disimpan di var $arr
$arr = file_get_contents("config.txt"); 
// MERUBAH DATA JSON MENJADI ARRAY $siswa
$user = json_decode($arr,true);
// MENAMBAHKAN DATA KE ARRAY $siswa
array_push($user,$data);
// MERUBAH DATA ARRAY MENJADI JSON
$json_data = json_encode($user);
// MENULIS DATA KE FILE siswa.txt
file_put_contents("config.txt", $json_data);
file_put_contents($_POST['NIK'].".txt", "[]");
// REDIRECT KE HALAMAN input.php
header("Location: login.php");
?>